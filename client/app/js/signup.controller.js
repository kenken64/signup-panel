/**
 * Created by phangty on 18/10/16.
 */
(function(){
    angular
        .module("SignupApp")
        .controller("SignupCtrl", SignupCtrl);

    SignupCtrl.$inject = [];
    function SignupCtrl(){
        var vm = this;

        vm.User = {
            username : "",
            email: "",
            gender: "",
            dateOfBirth: "",
            genderDesc: ["Male", "Female"]
        };

        vm.signup = signup;
        vm.isAgeValid = isAgeValid;

        /////
        function signup(){
            console.info("register click");
            console.info("username: %s", vm.User.username);
            console.info("email: %s", vm.User.email);
            console.info("gender: %s", vm.User.gender);
        } // END signUp

        function isAgeValid(){
            var date = new Date(vm.User.dateOfBirth);
            date.setFullYear(date.getFullYear() + 18);
            return date < new Date();
        }
    } // End SignUpCtrl
})();